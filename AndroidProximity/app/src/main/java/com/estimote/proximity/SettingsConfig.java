package com.estimote.proximity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class SettingsConfig extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_config);


        Button sendButton = findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText urlToSend = findViewById(R.id.URLTextEdit);
                String url = urlToSend.getText().toString();

                EditText nameUser = findViewById(R.id.NameTextEdit);
                String nameUserText = nameUser.getText().toString();

                RadioButton dist1 = findViewById(R.id.distance1Radio);
                RadioButton dist2 = findViewById(R.id.distance2Radio);
                RadioButton dist3 = findViewById(R.id.distance3Radio);
                RadioButton distP = findViewById(R.id.distancePRadio);



                EditText distPTextEdit = findViewById(R.id.distancePTextEdit);
                String distPText = distPTextEdit.getText().toString();

                if(!url.isEmpty()
                    && !nameUserText.isEmpty()
                    && (dist1.isChecked()
                        || dist2.isChecked()
                        || dist3.isChecked()
                        || (distP.isChecked() && !distPText.isEmpty()))){
                    //send data
                    double distance = -1;
                    try{
                        if(dist1.isChecked())
                            distance =  Double.parseDouble(dist1.getText().toString());
                        if(dist2.isChecked())
                            distance =  Double.parseDouble(dist2.getText().toString());
                        if(dist3.isChecked())
                            distance =  Double.parseDouble(dist3.getText().toString());
                        if(distP.isChecked())
                            distance =  Double.parseDouble(distPText);
                        Log.d("setting activity", String.valueOf(distance));
                    }catch(Exception ex){
                        Toast.makeText(view.getContext(), getText(R.string.ErrorSendSettings), Toast.LENGTH_LONG).show();
                        return;
                    }
                    if(distance == -1){
                        Toast.makeText(view.getContext(), getText(R.string.ErrorSendSettings), Toast.LENGTH_LONG).show();
                        return;
                    }


                    Intent intent = new Intent(view.getContext(), MainActivity.class);
                    intent.putExtra("name",nameUserText);
                    intent.putExtra("url",url);
                    intent.putExtra("distance",distance);
                    view.getContext().startActivity(intent);
                }
                else{
                    //send error
                    Toast.makeText(view.getContext(), getText(R.string.ErrorSendSettings), Toast.LENGTH_LONG).show();
                }
            }

        });
    }
}
