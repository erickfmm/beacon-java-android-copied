package com.estimote.proximity.Persistence.Amazon;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBIndexHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBIndexRangeKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBRangeKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;

import java.util.List;
import java.util.Map;
import java.util.Set;

@DynamoDBTable(tableName = "beaconsmla-mobilehub-1326480574-ProximityEventAWS")

public class ProximityEventAWSDO {
    private String _username;
    private Double _timeEv;
    private String _color;
    private String _longid;
    private String _scene;
    private Double _timeInterval;
    private String _type;

    @DynamoDBHashKey(attributeName = "username")
    @DynamoDBAttribute(attributeName = "username")
    public String getUsername() {
        return _username;
    }

    public void setUsername(final String _username) {
        this._username = _username;
    }
    @DynamoDBRangeKey(attributeName = "timeEv")
    @DynamoDBAttribute(attributeName = "timeEv")
    public Double getTimeEv() {
        return _timeEv;
    }

    public void setTimeEv(final Double _timeEv) {
        this._timeEv = _timeEv;
    }
    @DynamoDBAttribute(attributeName = "color")
    public String getColor() {
        return _color;
    }

    public void setColor(final String _color) {
        this._color = _color;
    }
    @DynamoDBAttribute(attributeName = "longid")
    public String getLongid() {
        return _longid;
    }

    public void setLongid(final String _longid) {
        this._longid = _longid;
    }
    @DynamoDBAttribute(attributeName = "scene")
    public String getScene() {
        return _scene;
    }

    public void setScene(final String _scene) {
        this._scene = _scene;
    }
    @DynamoDBAttribute(attributeName = "timeInterval")
    public Double getTimeInterval() {
        return _timeInterval;
    }

    public void setTimeInterval(final Double _timeInterval) {
        this._timeInterval = _timeInterval;
    }
    @DynamoDBAttribute(attributeName = "type")
    public String getType() {
        return _type;
    }

    public void setType(final String _type) {
        this._type = _type;
    }

}
