package com.estimote.proximity;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

import com.estimote.mustard.rx_goodness.rx_requirements_wizard.Requirement;
import com.estimote.mustard.rx_goodness.rx_requirements_wizard.RequirementsWizardFactory;
import com.estimote.proximity.Persistence.AppDatabase;
import com.estimote.proximity.estimote.ProximityContentAdapter;
import com.estimote.proximity.estimote.ProximityContentManager;
import com.estimote.proximity.Persistence.ProximityEvent;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.config.AWSConfiguration;


import com.amazonaws.mobile.client.AWSStartupHandler;
import com.amazonaws.mobile.client.AWSStartupResult;


import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

//
// Running into any issues? Drop us an email to: contact@estimote.com
//

public class MainActivity extends AppCompatActivity {

    private ProximityContentManager proximityContentManager;
    private ProximityContentAdapter proximityContentAdapter;
    private double distance;


    public static ArrayList<ProximityEvent> allEvents = new ArrayList<>();
    public static Hashtable<String, Timestamp> activeBeacons = new Hashtable<>();

    // Declare a DynamoDBMapper object
    DynamoDBMapper dynamoDBMapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        proximityContentAdapter = new ProximityContentAdapter(this);
        GridView gridView = findViewById(R.id.gridView);
        gridView.setAdapter(proximityContentAdapter);

        final String url = getIntent().getStringExtra("url");
        final String nameUserText = getIntent().getStringExtra("name");
        final double distance = getIntent().getDoubleExtra("distance", 1.5);
        this.distance = distance;

        Log.d("main intents", url);
        Log.d("main intents", nameUserText);
        Log.d("main intents", String.valueOf(distance));


        Button sendButton = findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                for(ProximityEvent ev : allEvents){
                    Log.d("app", ev.getColor()+"; "+ev.getTypeEvent().name()+"; "+ev.getTimeEvent().toString());
                }
                for(String beacName : activeBeacons.keySet()){
                    Log.d("app", "active: "+beacName);
                }
                try{
                    connectToDynamoDB();
                }catch(Exception e){
                    ShowError("Error al conectar a Amazon, intente nuevamente.");
                    return;
                }

                new Thread(new Runnable() {
                    public void run() {
                        //Aquí ejecutamos nuestras tareas costosas
                        try {
                            Thread.sleep(4000);
                            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                                    AppDatabase.class, "mydb").fallbackToDestructiveMigration().build();

                            int i = 0;
                            List<ProximityEvent> dbEvs = db.proximityEventDao().getAll();
                            Log.d("app", "DB Elements size: "+dbEvs.size());
                            for (ProximityEvent ev : dbEvs) {
                                //Map<String, String> params = makeParams(ev, nameUserText);
                                try {
                                    sendDataToAmazon(ev, nameUserText);
                                    //int res = SendPostData.getInstance().post(url,
                                    //        SendPostData.getInstance().makeJSONString(params));
                                    //if (res != 200)
                                    //    throw new Exception("Post in saved events received code: " + res);
                                    db.proximityEventDao().delete(ev);
                                } catch (Exception e) {
                                    //db.proximityEventDao().insertAll(ev);
                                    Log.d("error", "Error sending post saved event: "+i );
                                    e.printStackTrace();
                                    ShowError("Error sending post saved event: "+i);
                                    //Toast.makeText(MainActivity.this, "Error sending post saved event: "+i, Toast.LENGTH_SHORT).show();
                                }
                                i++;
                            }

                            i = 0;
                            //MainActivity.allEvents.add((new ProximityEvent("34343", "hello", "bye bye", new Timestamp(0), new Timestamp(0), TypeEvent.Enter)));
                            for (ProximityEvent ev : MainActivity.allEvents) {
                                if (ev.isSended())
                                    continue;
                                //Map<String, String> params = makeParams(ev, nameUserText);

                                try {
                                    sendDataToAmazon(ev, nameUserText);
                                    //int res = SendPostData.getInstance().post(url,
                                    //        SendPostData.getInstance().makeJSONString(params));
                                    //if (res != 200)
                                    //    throw new Exception("Post in new events received code: " + res);
                                    ev.setSended(true);
                                } catch (Exception e) {
                                    Log.d("error", "Error sending post new event: "+i);
                                    Log.d("error", e.getMessage());
                                    if(e.getCause() != null)
                                        Log.d("error", "cause: "+e.getCause().getMessage());
                                    db.proximityEventDao().insertAll(ev);
                                    e.printStackTrace();
                                    ShowError("Error sending post new event: "+i);
                                    //Toast.makeText(MainActivity.this, "Error sending post new event: "+i, Toast.LENGTH_SHORT).show();
                                }
                                System.out.print(ev.getColor());
                                System.out.print("; ");
                                //System.out.print(ev.getTimeEvent().toString());
                                //System.out.print("; ");
                                System.out.print(ev.getTypeEvent().name());
                                System.out.print("; ");
                                System.out.print(ev.getShortId());
                                System.out.print("; ");
                                System.out.print(ev.getLongId());
                                System.out.println();
                                i++;
                            }
                        }catch(Exception ex3){
                            Log.d("errorout_inthread_main",ex3.getMessage());
                            //ShowError(ex3.getMessage());
                            //if(ex3.getCause() != null)
                                //ShowError(ex3.getCause().getMessage());
                        }
                    }

                }).start();

            }
            });


        RequirementsWizardFactory
                .createEstimoteRequirementsWizard()
                .fulfillRequirements(this,
                        new Function0<Unit>() {
                            @Override
                            public Unit invoke() {
                                Log.d("app", "requirements fulfilled");
                                startProximityContentManager();
                                return null;
                            }
                        },
                        new Function1<List<? extends Requirement>, Unit>() {
                            @Override
                            public Unit invoke(List<? extends Requirement> requirements) {
                                Log.e("app", "requirements missing: " + requirements);
                                return null;
                            }
                        },
                        new Function1<Throwable, Unit>() {
                            @Override
                            public Unit invoke(Throwable throwable) {
                                Log.e("app", "requirements error: " + throwable);
                                return null;
                            }
                        });
    }

    private void ShowError(String message){
        Toast.makeText(this,message, Toast.LENGTH_LONG).show();
    }

    private Map<String, String> makeParams(ProximityEvent ev, String nameUserText){
        Map<String, String> params = new HashMap<>();
        params.put("color", ev.getColor());
        params.put("time", ev.getTimeEvent().toString());
        params.put("timeInterval", ev.getTotalTimeInterval() != null ? ev.getTotalTimeInterval().toString() : "");
        params.put("type", ev.getTypeEvent().name());
        params.put("shortId", ev.getShortId());
        params.put("longId", ev.getLongId());
        params.put("nameUser", nameUserText);
        return params;
    }

    private void connectToDynamoDB() {
        if(dynamoDBMapper != null){
            return;
        }
        Log.d("dynamo", "to build create");
        AWSMobileClient.getInstance().initialize(this, new AWSStartupHandler() {
            @Override
            public void onComplete(AWSStartupResult awsStartupResult) {
                Log.d("dynamo", "AWSMobileClient is instantiated and you are connected to AWS!");
                AWSCredentialsProvider  credentialsProvider = AWSMobileClient.getInstance().getCredentialsProvider();
                Log.d("dynamo", "credentials created succesfully: "+(credentialsProvider!=null));
                AWSConfiguration  configuration = AWSMobileClient.getInstance().getConfiguration();
                Log.d("dynamo", "configuracion obtained: "+(configuration!=null));
                if(configuration!= null){
                    Log.d("dynamo", configuration.getConfiguration());
                    Log.d("dynamo", configuration.getUserAgent());
                    Log.d("dynamo", configuration.toString());
                }
                else
                    Log.d("dynamo", "configuration is null");
                // Add code to instantiate a AmazonDynamoDBClient
                AmazonDynamoDBClient dynamoDBClient = new AmazonDynamoDBClient(credentialsProvider);
                Log.d("dynamo", "client obtained: "+(dynamoDBClient!=null));
                dynamoDBMapper = DynamoDBMapper.builder()
                        .dynamoDBClient(dynamoDBClient)
                        .awsConfiguration(configuration)
                        .build();
                Log.d("dynamo", "mapper was created succesfully");
            }
        }).execute();
        Log.d("dynamo", "created succesfully");
    }

    private void sendDataToAmazon(ProximityEvent ev, String nameUserText) throws Exception {
        if(dynamoDBMapper == null){
            Log.d("dynamo", "mapper is null");
            throw new Exception("Mapper is Null");
        }
        final com.estimote.proximity.Persistence.Amazon.ProximityEventAWSDO newsItem = new com.estimote.proximity.Persistence.Amazon.ProximityEventAWSDO();

        newsItem.setUsername(nameUserText);
        newsItem.setColor(ev.getColor());
        newsItem.setLongid(ev.getLongId());
        newsItem.setTimeEv((double)(ev.getTimeEvent().getTime()));
        newsItem.setTimeInterval(ev.getTotalTimeInterval() != null ? (double)(ev.getTotalTimeInterval().getTime()) : 0);
        newsItem.setType(ev.getTypeEvent().toString());
        newsItem.setScene("Default");
        Log.d("dynamo", "entity setted");


                try {
                    dynamoDBMapper.save(newsItem);
                    Log.d("dynamo", "item saved");
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dynamo", "Error at saving item");
                    Log.d("dynamo", e.getMessage());
                    if(e.getCause() != null)
                        Log.d("dynamo", e.getCause().getMessage());
                    throw new Exception("Error saving at dynamodb");
                }

                // Item saved

    }

    private void startProximityContentManager() {
        proximityContentManager = new ProximityContentManager(this, proximityContentAdapter, ((MyApplication) getApplication()).cloudCredentials, this.distance);
        proximityContentManager.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (proximityContentManager != null)
            proximityContentManager.stop();
    }

}
