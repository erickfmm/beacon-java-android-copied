package com.estimote.proximity;

import android.util.Log;

import java.io.IOException;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendPostData {
    private OkHttpClient client;
    private static SendPostData _instance;

    public static SendPostData getInstance(){
        if(_instance == null){
            _instance = new SendPostData();
        }
        return _instance;
    }

    private SendPostData(){
        client = new OkHttpClient();
    }

    private static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    public int post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            System.out.println("---------------------------------------------");
            System.out.println("---------------------------------------------");
            System.out.println(response.code());
            System.out.println("---------------------------------------------");
            System.out.println("---------------------------------------------");
            System.out.println(response.body().string());
            System.out.println("---------------------------------------------");
            System.out.println("---------------------------------------------");
            return response.code();
        }
    }

    public String makeJSONString(Map<String, String> values){
        String json = "{";
        int i = 0;
        for(String keytag : values.keySet()){
            i++;
            json += '"'+keytag+'"';
            json += ": ";
            json += '"'+values.get(keytag)+'"';
            if(i < values.size())
                json += ",";
        }
        json += "}";
        System.out.println(json);
        return json;
    }
}
