package com.estimote.proximity.Persistence;

import android.arch.persistence.room.TypeConverter;

import java.sql.Timestamp;

public class Converters {
    @TypeConverter
    public static String TimeStampToStr(Timestamp timestamp){
        return timestamp.toString();
    }

    @TypeConverter
    public static Timestamp StrToTimestamp(String string){
        return Timestamp.valueOf(string);
    }

    @TypeConverter
    public static TypeEvent StrToTypeEv(String tev){
        return TypeEvent.valueOf(tev);
    }

    @TypeConverter
    public static String TypeEvToStr(TypeEvent tev){
        return tev.name();
    }
}
