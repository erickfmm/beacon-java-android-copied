package com.estimote.proximity.estimote;

import android.app.Notification;
import android.content.Context;
import android.util.Log;

import com.estimote.internal_plugins_api.cloud.CloudCredentials;
import com.estimote.internal_plugins_api.cloud.proximity.ProximityAttachment;
import com.estimote.proximity.MainActivity;
import com.estimote.proximity.Persistence.ProximityEvent;
import com.estimote.proximity.Persistence.TypeEvent;
import com.estimote.proximity_sdk.proximity.ProximityObserver;
import com.estimote.proximity_sdk.proximity.ProximityObserverBuilder;
import com.estimote.proximity_sdk.proximity.ProximityZone;

import java.util.ArrayList;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

import java.sql.Timestamp;

//
// Running into any issues? Drop us an email to: contact@estimote.com
//

public class ProximityContentManager {

    private Context context;
    private ProximityContentAdapter proximityContentAdapter;
    private CloudCredentials cloudCredentials;
    private ProximityObserver.Handler proximityObserverHandler;
    private double distance;


    public ProximityContentManager(Context context, ProximityContentAdapter proximityContentAdapter, CloudCredentials cloudCredentials, double distance) {
        this.context = context;
        this.proximityContentAdapter = proximityContentAdapter;
        this.cloudCredentials = cloudCredentials;
        this.distance = distance;
    }

    public void start() {

        ProximityObserver proximityObserver = new ProximityObserverBuilder(context, cloudCredentials)
                .withLowLatencyPowerMode()
                .withScannerInForegroundService(new Notification())
                .withOnErrorAction(new Function1<Throwable, Unit>() {
                    @Override
                    public Unit invoke(Throwable throwable) {
                        Log.e("app", "proximity observer error: " + throwable);
                        return null;
                    }
                })
                .build();

        ProximityZone zone = proximityObserver.zoneBuilder()
                .forAttachmentKeyAndValue("roberto-munoz-s-proximity--nej", "example-proximity-zone")
                .inCustomRange(this.distance)
                .withOnChangeAction(new Function1<List<? extends ProximityAttachment>, Unit>() {
                    @Override
                    public Unit invoke(List<? extends ProximityAttachment> attachments) {
                        Log.d("app", "invoco cambios :)");

                        List<ProximityContent> nearbyContent = new ArrayList<>(attachments.size());

                        if(attachments.isEmpty()){
                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                            for(String beacName : MainActivity.activeBeacons.keySet()){
                                System.out.println("exited: "+beacName);
                                Timestamp timeInterval = new Timestamp(
                                        timestamp.getTime() - MainActivity.activeBeacons.get(beacName).getTime());
                                MainActivity.allEvents.add(new ProximityEvent(beacName,
                                        Utils.getShortIdentifier(beacName),
                                        "all_exited_color_not_known",
                                        timestamp,
                                        timeInterval,
                                        TypeEvent.Exit));
                            }
                            MainActivity.activeBeacons.clear();
                        }

                        for (ProximityAttachment attachment : attachments) {
                            String title = attachment.getPayload().get("roberto-munoz-s-proximity--nej/title");
                            if (title == null) {
                                title = "unknown";
                            }
                            System.out.println("--------------------------------");
                            System.out.println("--------------------------------");
                            System.out.println("theeeeeeee payloadsssss");
                            for(String keytag : attachment.getPayload().keySet() ){
                                System.out.println(keytag+": "+attachment.getPayload().get(keytag));
                            }
                            System.out.println("--------------------------------");
                            System.out.println("--------------------------------");
                            System.out.println("has: "+attachment.hasAttachment());
                            String subtitle = Utils.getShortIdentifier(attachment.getDeviceId());

                            nearbyContent.add(new ProximityContent(title, subtitle));

                            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                            if(MainActivity.activeBeacons.containsKey(attachment.getDeviceId())){
                                System.out.println("exited: "+title);
                                Timestamp timeInterval = new Timestamp(
                                        timestamp.getTime() - MainActivity.activeBeacons.get(attachment.getDeviceId()).getTime());
                                MainActivity.allEvents.add(new ProximityEvent(attachment.getDeviceId(),
                                        Utils.getShortIdentifier(attachment.getDeviceId()),
                                        title,
                                        timestamp,
                                        timeInterval,
                                        TypeEvent.Exit));

                                MainActivity.activeBeacons.remove(attachment.getDeviceId());
                            }else{
                                System.out.println("entered: "+title);
                                MainActivity.allEvents.add(new ProximityEvent(attachment.getDeviceId(),
                                        Utils.getShortIdentifier(attachment.getDeviceId()),
                                        title,
                                        timestamp,
                                        null,
                                        TypeEvent.Enter));

                                MainActivity.activeBeacons.put(attachment.getDeviceId(), timestamp);
                            }
                            System.out.println("holiwis");
                        }

                        proximityContentAdapter.setNearbyContent(nearbyContent);
                        proximityContentAdapter.notifyDataSetChanged();

                        return null;
                    }
                })/*
                .withOnEnterAction(new Function1<ProximityAttachment, Unit>() {
                    @Override
                    public Unit invoke(ProximityAttachment attachment) {
                        System.out.println("entered");
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        String title = attachment.getPayload().get("roberto-munoz-s-proximity--nej/title");
                        if (title == null) {
                            title = "unknown";
                        }
                        System.out.println(title);
                        MainActivity.allEvents.add(new ProximityEvent(attachment.getDeviceId(),
                                Utils.getShortIdentifier(attachment.getDeviceId()),
                                title,
                                timestamp,
                                TypeEvent.Enter));
                        return null;
                    }})
                .withOnExitAction(new Function1<ProximityAttachment, Unit>() {
                    @Override
                    public Unit invoke(ProximityAttachment attachment) {
                        System.out.println("exited");
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        String title = attachment.getPayload().get("roberto-munoz-s-proximity--nej/title");
                        if (title == null) {
                            title = "unknown";
                        }
                        System.out.println(title);
                        MainActivity.allEvents.add(new ProximityEvent(attachment.getDeviceId(),
                                Utils.getShortIdentifier(attachment.getDeviceId()),
                                title,
                                timestamp,
                                TypeEvent.Exit));
                        return null;
                    }})*/
                .create();
        proximityObserver.addProximityZone(zone);

        proximityObserverHandler = proximityObserver.start();
    }

    public void stop() {
        proximityObserverHandler.stop();
    }
}
