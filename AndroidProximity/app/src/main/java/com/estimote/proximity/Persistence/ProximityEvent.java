package com.estimote.proximity.Persistence;

import java.sql.Timestamp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class ProximityEvent {
    @PrimaryKey
    private int uid;
    @ColumnInfo(name = "long_id")
    private String longId;
    @ColumnInfo(name = "short_id")
    private String shortId;
    @ColumnInfo(name = "color")
    private String color;
    @ColumnInfo(name = "time")
    private Timestamp timeEvent;
    @ColumnInfo(name = "totalTimeInterval")
    private Timestamp totalTimeInterval;
    @ColumnInfo(name = "type")
    private TypeEvent typeEvent;
    @Ignore
    private boolean isSended;

    public ProximityEvent(String longId, String shortId, String color, Timestamp timeEvent, Timestamp totalTimeInterval, TypeEvent typeEvent){
        this.longId = longId;
        this.shortId = shortId;
        this.color = color;
        this.timeEvent = timeEvent;
        this.totalTimeInterval = totalTimeInterval;
        this.typeEvent = typeEvent;
        this.isSended = false;
    }

    public String getLongId() {
        return longId;
    }

    public void setLongId(String longId) {
        this.longId = longId;
    }

    public String getShortId() {
        return shortId;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Timestamp getTimeEvent() {
        return timeEvent;
    }

    public void setTimeEvent(Timestamp timeEvent) {
        this.timeEvent = timeEvent;
    }

    public TypeEvent getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(TypeEvent typeEvent) {
        this.typeEvent = typeEvent;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }


    public boolean isSended() {
        return isSended;
    }

    public void setSended(boolean sended) {
        isSended = sended;
    }



    public Timestamp getTotalTimeInterval() {
        return totalTimeInterval;
    }

    public void setTotalTimeInterval(Timestamp totalTimeInterval) {
        this.totalTimeInterval = totalTimeInterval;
    }



}