package com.estimote.proximity.Persistence;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.estimote.proximity.Persistence.ProximityEvent;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ProximityEventDao {
    @Query("SELECT * FROM ProximityEvent")
    List<ProximityEvent> getAll();

    @Insert(onConflict = REPLACE)
    void insertAll(ProximityEvent... proximityEvent);

    @Delete
    void delete(ProximityEvent proximityEvent);
}
